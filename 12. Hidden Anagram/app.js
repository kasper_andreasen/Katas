const hiddenAnagram = (text, phrase) => {
  const textLetters = toLowerCaseLetters(text);
  const phraseLetters = toLowerCaseLetters(phrase);
  const chunks = chunker(textLetters, phraseLetters);

  let value;

  while ((value = chunks.next().value)) {
    if (match(value, phraseLetters)) return value;
  }

  return "noutfond";
};

const toLowerCaseLetters = (text) => text.toLowerCase().replace(/[^a-z]/g, "");
const chunker = function* (sentence, phrase) {
  let i = 0;
  while (sentence.length - i >= phrase.length) {
    yield sentence.substr(i++, phrase.length);
  }
};

const match = ([letter, ...word], phrase) =>  phrase.indexOf(letter) === -1
    ? phrase.length
      ? false
      : true
    : match(word, phrase.replace(letter, ""));

// Test Cases
console.log(hiddenAnagram("An old west action hero actor", "Clint Eastwood"));
console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM"));
console.log(hiddenAnagram("D e b90it->?$ (c)a r...d,,#~", "bad credit"));
console.log(hiddenAnagram("Bright is the moon", "Bongo mirth"));
