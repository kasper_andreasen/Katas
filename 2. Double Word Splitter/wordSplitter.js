function doubleLetterSplitter(word) {
    if (typeof word === "string") {
        let prevChar = null;
        let newWord = "";
        for (c of word) {
            if (prevChar === c) {
                newWord += " ";
            }
            newWord += c;
            prevChar = c;
        }
        const res = newWord.split(" ");
        if (res.length > 1) {
            for (str of res) {
                console.log(str);
            } 
        } else {
            console.log("I am an empty array.");
        }
    }
}
doubleLetterSplitter("Mississippi");