const testArrayOne = [1, [2, 3]];
const testArrayTwo = [1, [2, [3, 4]]];
const testArrayThree = [1, [2, [3, [4, [5, 6]]]]];
const testArrayFour = [1, [2], 1, [2], 1];
const testArrayCustom = [1, 2, [1, 2, 3], [1, [2, 3, [4]]]];
const testArrayEdgeCase = [];

const testArrayEdgeCaseTwo = [[[[[[[["a",[[[[[[[[[[1, 2, 3], 6, 7]]]]], 8, 0, 1]]]]], 1, 2]]]]]]];

// The fully written out version
function getLength(array) {
    let count = 0;
    for (const value of array) {
        if (Array.isArray(value)) {
            // Recurse
            count += getLength(value);
        } else {
            // Count
            ++count;
        }
    }
    return count;
}

// This solution is really interesting. The flat function, flattens any array it finds into the parent
// array, and giving it Infinity as the argument, makes it keep going. I will recurse forever.
const getLengthShort = array => array.flat(Infinity).length;

console.log("Testing with the fully fleshed out function");
console.log(getLength(testArrayOne));
console.log(getLength(testArrayTwo));
console.log(getLength(testArrayThree));
console.log(getLength(testArrayFour));
console.log(getLength(testArrayCustom));
console.log(getLength(testArrayEdgeCase));
console.log(getLength(testArrayEdgeCaseTwo));

console.log("Testing with the oneliner function")
console.log(getLengthShort(testArrayOne));
console.log(getLengthShort(testArrayTwo));
console.log(getLengthShort(testArrayThree));
console.log(getLengthShort(testArrayFour));
console.log(getLengthShort(testArrayCustom));
console.log(getLengthShort(testArrayEdgeCase));
console.log(getLengthShort(testArrayEdgeCaseTwo));