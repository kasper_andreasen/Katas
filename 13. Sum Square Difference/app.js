const SumSquareDifference = (naturalNumber) => {
    if (naturalNumber < 0) return "Not a natural number, try again bub!";
    let sumOfSquares = 0;
    let middleSum = 0;
    for (let i = 0; i <= naturalNumber; i++) {
        sumOfSquares += i**2;
    }
    for (let i = 0; i <= naturalNumber; i++) {
        middleSum += i;
    }
    let squaredSum = middleSum**2;
    return squaredSum - sumOfSquares;
}
