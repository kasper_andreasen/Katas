function fizzBuzzSingle(n) {
    console.log("This is FizzBuzz, with just an end value.")
    for (let i = 1; i <= n; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            console.log("FizzBuzz");
        } else if (i % 3 === 0) {
            console.log("Fizz");
        } else if (i % 5 === 0) {
            console.log("Buzz");
        } else {
            console.log(i);
        }
    }
}

function fizzBuzzTwoValues(start, end) {
    console.log("This is the FizzBuzz with a start and an end value.");
    for (let i = start; i <= end; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            console.log("FizzBuzz");
        } else if (i % 3 === 0) {
            console.log("Fizz");
        } else if (i % 5 === 0) {
            console.log("Buzz");
        } else {
            console.log(i);
        }
    }
}
fizzBuzzSingle(20);
fizzBuzzTwoValues(11, 328);