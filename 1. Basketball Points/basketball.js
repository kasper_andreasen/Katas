
// // The most barebones version
function basketScore(twoPointers, threePointers) {
    console.log((twoPointers * 2) + (threePointers * 3));
}

// // A bit more fleshed out. Looks ugly though
function basketScored(twoPointers, threePointers) {
    let two = twoPointers;
    let three = threePointers;
    two *= 2;
    three *= 3;
    let sum = two + three;
    console.log(sum);
}

basketScore(38, 8);
basketScored(38, 8);


// A weird example, by Tomas Mrvos where a variable is declared as a return type.
// Can't really tell why this works.
function points(twoPointers, threePointers) {
    finalPoints = ((twoPointers * 2) + (threePointers * 3))}
    points(60,10);
    console.log("Final points: " + finalPoints);