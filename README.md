# This is just a repo for the different code KATA's. 
It's primary use is for me to refamiliarize with GIT and working with it. :)

## Deployment for anyone interested

The first few of these katas have an index.html. On these you can deploy them using the LiveServer plugin
of Visual Studio Code, or alternatively, you can just run the index.html file and open your dev tools.

In the later katas, I am only doing an app.js, and to run this you need node installed. Open a terminal in
the folder where the app.js resides, and then run "node app.js", and it will output.
