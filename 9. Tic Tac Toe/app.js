const gameOne = [
    ["X", "O", "X"],
    ["O", "X", "O"],
    ["O", "X", "X"]
];


const gameTwo = [
    ["O", "O", "O"],
    ["O", "X", "X"],
    ["E", "X", "X"]
];

const gameThree = [
    ["X", "X", "O"],
    ["O", "O", "X"],
    ["X", "X", "O"]
];

const matches = [
    [1, 4, 7],
    [2, 5, 8],
    [3, 6, 9],
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [1, 5, 9],
    [3, 5, 7]
]

const solver = (board, matches) => {
    // build map
    let counter = 1;
    const map = {};

    // convert array to object
    for (let row of board) {
        for (let char of row) {
            map[counter] = char;
            counter++;
        }
    }

    for (let match of matches) {
        // create a new array with the values
        const arr = [...map[match[0]], map[match[1]], map[match[2]]];
        // check for duplicates
        const arr2 = [...new Set(arr)].join("");
        // console log if length is 1
        if (arr2.length === 1) {
            console.log(`match found at set: ${match}`)
        } else {
            console.log("Draw");
        }

    }
}

// solver(gameOne, matches);
// solver(gameTwo, matches);
solver(gameThree, matches);