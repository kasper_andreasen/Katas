// countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞ 2
// countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞ 1
// countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞ 0
// countBoomerangs([1, 7, 1, 7, 1, 7, 1]) ➞ 5

const countBoomerangs = (array) => {
    let count = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] !== array[i + 1] && array[i] === array[i + 2]) {
            count++;
        }
    }
    return count;
}

console.log("Number of Boomerangs: ", countBoomerangs([9, 5, 9, 5, 1, 1, 1]));
console.log("Number of Boomerangs: ", countBoomerangs([5, 6, 6, 7, 6, 3, 9]));
console.log("Number of Boomerangs: ", countBoomerangs([4, 4, 4, 9, 9, 9, 9]));
console.log("Number of Boomerangs: ", countBoomerangs([1, 7, 1, 7, 1, 7, 1]));