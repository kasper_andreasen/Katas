function closeToFizzBuzz(number) {
    let sum = 0;
    console.log("Number passed", number);
    for (let i = 0; i < number; i++) {
        if (i % 5 === 0 && i % 3 === 0) {
            // do nothing here!
        } else if (i % 3 === 0) {
            sum += parseFloat(i);
            console.log("Modulus 3", i, sum);
        } else if (i % 5 === 0) {
            sum += parseFloat(i);
            console.log("Modulus 5", i, sum);
        }
    }
    return sum;
}
console.log(closeToFizzBuzz(1000));