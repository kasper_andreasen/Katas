// input = string to encrypt, rotationFactor = shift value
// input = 'Abebss' and rotationFactor = 13 will produce 'Noroff'
const caesarCipher = function (input, rotationFactor) {
    let result = '';

    for (let i = 0; i < input.length; i++) {

        let charCode = input[i].charCodeAt();
        // check that charCode is a lowercase letter; automatically ignores non-letters
        if (charCode > 96 && charCode < 123) {

            charCode += rotationFactor % 26 // makes it work with numbers greater than 26 to maintain correct shift
            // if shift passes 'z', resets to 'a' to maintain looping shift
            if (charCode > 122) {
                charCode = (charCode - 122) + 96;
                // same as previous, but checking shift doesn't pass 'a' when shifting negative numbers
            } else if (charCode < 97) {
                charCode = (charCode - 97) + 123;
            }
        }

        if (charCode > 64 && charCode < 91) {

            charCode += rotationFactor % 26

            if (charCode > 90) {
                charCode = (charCode - 90) + 64;
            } else if (charCode < 65) {
                charCode = (charCode - 65) + 91;
            }
        }

        result += String.fromCharCode(charCode);
    }
    return result;
}                                                                                                      
console.log("____________________________________________________________________________________________________");
console.log("A friend in need is a friend indeed      ➞    ", caesarCipher("A friend in need is a friend indeed", 10), "➞  Shifted by 10|");
console.log("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
console.log("______________________________________________________________________________________________________");
console.log("How much wood could a woodchuck chuck    ➞    ", caesarCipher("How much wood could a woodchuck chuck", 20), "➞  Shifted by 20|");
console.log("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
console.log("________________________________________________________________________________________________");
console.log("If a woodchuck could chuck wood          ➞    ", caesarCipher("If a woodchuck could chuck wood", 96), "➞  Shifted by 96|");
console.log("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");
console.log("________________________________________________________________________________________________________");
console.log("English motherfucker, do you speak it?   ➞    ", caesarCipher("English motherfucker, do you speak it?", -20), "➞  Shifted by -20|");
console.log("¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯");

console.log(caesarCipher("Abebss", 13));

